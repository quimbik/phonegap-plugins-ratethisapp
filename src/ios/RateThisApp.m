//
//  RateThisApp.m
//
//  Created by Alessandro Jatoba on 4/3/13.
//
//

#import "RateThisApp.h"

@implementation RateThisApp

- (void)rate:(CDVInvokedUrlCommand*)command
{
	NSString* appid = (NSString*)[command.arguments objectAtIndex:0];
    
    static NSString *const iOS7AppStoreURLFormat = @"itms-apps://itunes.apple.com/app/id%@?at=10l6dK";
    static NSString *const iOSAppStoreURLFormat = @"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%@&at=10l6dK";
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:([[UIDevice currentDevice].systemVersion floatValue] >= 7.0f)? iOS7AppStoreURLFormat: iOSAppStoreURLFormat, appid]];
    
    [[UIApplication sharedApplication] openURL:url];
}

@end
