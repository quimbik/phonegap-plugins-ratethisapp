//
//  RateThisApp.h
//
//  Created by Alessandro Jatoba on 4/3/13.
//
//

#import <Cordova/CDV.h>

@interface RateThisApp : CDVPlugin

- (void)rate:(CDVInvokedUrlCommand*)command;

@end