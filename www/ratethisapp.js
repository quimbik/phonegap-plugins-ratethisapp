var RateThisApp = function() {
};

RateThisApp.prototype.rate = function(appid)
{
    cordova.exec(null, null, "RateThisApp", "rate", [appid]);
};

//-------------------------------------------------------------------

if(!window.plugins) {
    window.plugins = {};
}

if (!window.plugins.ratethisapp) {
    window.plugins.ratethisapp = new RateThisApp();
}

if (module.exports) {
    module.exports = RateThisApp;
}